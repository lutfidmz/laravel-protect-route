<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role_id;
        $path = $request->path();

        $routeSuperAdmin = ['route-1', 'route-2', 'route-3', 'home'];
        $routeAdmin = ['route-2', 'route-3', 'home'];
        $routeGuest = ['route-3', 'home'];

        if ($role == 0 && in_array($path, $routeSuperAdmin)) {
            return $next($request);
        } elseif ($role == 1 && in_array($path, $routeAdmin)) {
            return $next($request);
        } elseif ($role == 2 && in_array($path, $routeGuest)) {
            return $next($request);
        }
        abort(403);
    }
}
