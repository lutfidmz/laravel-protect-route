<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('dateMiddleware');
    // }
    public function test()
    {
        return 'berhasil masuk';
    }

    public function test1()
    {
        return 'berhasil masuk ke test1';
    }

    public function admin()
    {
        return 'admin berhasil masuk';
    }

    public function route1()
    {
        return 'berhasil masuk ke route1';
    }

    public function route2()
    {
        return 'berhasil masuk ke route2';
    }

    public function route3()
    {
        return 'berhasil masuk ke route3';
    }
}
