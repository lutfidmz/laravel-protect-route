<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::middleware('dateMiddleware')->group(function () {
    Route::get('/test', 'TestController@test');
});
Route::get('/test1', 'TestController@test1');


Auth::routes();
Route::middleware(['auth', 'role'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin', 'TestController@admin');
    Route::get('/route-1', 'TestController@route1');
    Route::get('/route-2', 'TestController@route2');
    Route::get('/route-3', 'TestController@route3');
});
